<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Helpers\JwtAuth;
use Illuminate\Auth\Access\Response;

class UserController extends Controller
{
    public function login(Request $request)
    {

        $jwtAuth = new JwtAuth();

        //Recibir POST

        $json = $request->input('json', null);
        $params = json_decode($json);

        $email = (!is_null($json) && isset($params->email)) ? $params->email : null;
        $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
        $getToken = (!is_null($json) && isset($params->getToken)) ? $params->getToken : null;

        //Ciframos la contraseña que nos llega

        $pwd = hash('sha256', $password);

        if (!is_null($email) && !is_null($password) && ($getToken == null || $getToken == 'false')) {

            $signup = $jwtAuth->signup($email, $pwd);
        } elseif ($getToken != null) {

            $signup = $jwtAuth->signup($email, $pwd, $getToken);
        } else {
            $signup = array(
                'status' => 'error',
                'message' => 'Envia tus datos por post'
            );
        }
        return response()->json($signup, 200);
    }
    public function register(Request $request)
    {
        //recogemos a través del post, si no lo mandan por defecto será null
        $json = $request->input('json', null);

        $params = json_decode($json);

        $nombre = (!is_null($json) && isset($params->name)) ? $params->name : null;
        $email = (!is_null($json) && isset($params->email)) ? $params->email : null;
        $role_id = (!is_null($json) && isset($params->role_id)) ? $params->role_id : null;
        $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
        $activo = (!is_null($json) && isset($params->Activo)) ? $params->Activo : null;

        //Ciframos la contraseña que nos llega
        $pwd = hash('sha256', $password);

        //Creramos el usuario
        $user = new User();

        $user->name = $nombre;
        $user->role_id = $role_id;
        $user->email = $email;
        $user->password = $pwd;
        $user->Activo =$activo;
        $user->save();
    }
    public function update($id, Request $request)
    {
        // $hash = $request->header('Authorization', null);

        // $jwtAuth = new JwtAuth();
        // $checkToken = $jwtAuth->checkToken($hash);

        // if ($checkToken) {
            //recoger parametros post

            $json = $request->input('json', null);
            $params = json_decode($json);
            //var_dump($params);
            //$name = (!is_null($json) && isset($params->name)) ? $params->name : null;
            //$email = (!is_null($json) && isset($params->email)) ? $params->email : null;
            $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
            //$role = (!is_null($json) && isset($params->role_id)) ? $params->role_id : null;

            $pwd = hash('sha256', $password);
            //actualizar el registro de usuarios
            $user = User::where(
                array(
                    'id' => $id,
                )
            )->first();
            //$user->name = $name;
            //$user->email = $email;
            $user->password = $pwd;
            // $user->Activo = 1;
            //$user->role_id = $role;
            $user->save();

            $data = array(
                'tarea' => $user,
                'status' => 'success',
                'code' => 200,
            );
        // } else {
        //     $data = array(
        //         'message' => 'Actualizado Incorrecto',
        //         'status' => 'error',
        //         'code' => 400,
        //     );
        // }

        return response()->json($data, 200);
    }
}
