<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\DB;

class EmpresarioController extends Controller
{
    public function index(Request $request)
    {
        //Consulta al modelo user para sacar los ususarios que son trabajadores
        $users = User::where('role_id', '=', 3)->get();
        return response()->json(array(
            'usuarios' => $users,
            'status' => 'success'
        ), 200);
    }

    public function show($id)
    {

        $users = DB::table('users')
            ->join('tarea_realizada', 'user_id', '=', 'users.id')
            ->join('tareas', 'tarea_realizada.id_tarea', '=', 'tareas.id')
            ->select('tarea_realizada.*', 'tareas.tarea')
            ->where('user_id', '=', $id)
            ->orderBy('tarea_realizada.fecha_realizada', 'desc')
            ->get();

        if (is_object($users)) {
            return response()->json(array(
                'usuario' => $users,
                'status' => 'success'
            ), 200);
        } else {
            return response()->json(array(
                'message' => 'Error usuario no encontrado',
                'status' => 'Error'
            ), 400);
        }
    }

    public function deleteWorkers($id, Request $request)
    {
        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //recoger parametros post

            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);

            //compruebo el usuario
            $user = User::find($id);
            $user->Activo = 0;
            $user->save();

            $data = array(
                'Usuario' => $user,
                'status' => 'Usuario dado de baja Correctamente',
                'code' => 200,
            );
        } else {
            $data = array(
                'message' => 'Error al dar de baja ',
                'status' => 'error',
                'code' => 400,
            );
        }

        return response()->json($data, 200);
    }


    //Metodo para volver a dar de alta a un trabajador
    public function activarWorkers($id, Request $request)
    {
        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //recoger parametros post

            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);

            //compruebo el usuario
            $user = User::find($id);
            $user->Activo = 1;
            $user->save();

            $data = array(
                'Usuario' => $user,
                'status' => 'Usuario dado de alta Correctamente',
                'code' => 200,
            );
        } else {
            $data = array(
                'message' => 'Error al dar de alta Incorrecto',
                'status' => 'error',
                'code' => 400,
            );
        }

        return response()->json($data, 200);
    }

    //Actualizar datos de los trabajadores
    public function update($id, Request $request)
    {
        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //recoger parametros post

            $json = $request->input('json', null);
            $params = json_decode($json);

            $name = (!is_null($json) && isset($params->name)) ? $params->name : null;
            $email = (!is_null($json) && isset($params->email)) ? $params->email : null;
            $password = (!is_null($json) && isset($params->password)) ? $params->password : null;

            $pwd = hash('sha256', $password);
            //actualizar el registro de usuarios
            //Aqui le impongo el rol de empleado por defecto por si el empresario se equivoca
            $user = User::where(
                array(
                    'id' => $id,
                )
            )->first();
            $user->name = $name;
            $user->email = $email;
            $user->password = $pwd;
            // $user->Activo = 1;
            $user->role_id = 3;
            $user->save();

            $data = array(
                'tarea' => $user,
                'status' => 'Actualizado Correcto',
                'code' => 200,
            );
        } else {
            $data = array(
                'message' => 'Actualizado Incorrecto',
                'status' => 'error',
                'code' => 400,
            );
        }

        return response()->json($data, 200);
    }
}
