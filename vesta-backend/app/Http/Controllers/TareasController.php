<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tarea;

use App\Tarea_Realizada;
use Illuminate\Support\Facades\Auth;
use App\Role;

class TareasController extends Controller
{

    public function getTareas(Request $request){

            $tareas = Tarea::all();
            return response()->json(array(
                'tareas' => $tareas,
                'status' => 'success'
            ), 200);

    }
    public function getRoles(Request $request){

        $role = Role::all();
        return response()->json(array(
            'roles' => $role,
            'status' => 'success'
        ), 200);

}

}
