<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use App\User;

use Illuminate\Support\Facades\DB;
class AdminController extends Controller
{
    public function index(Request $request)
    {
        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            echo "Index de Admin controller Autenticado";
            die();
        } else {
            echo "No autenticado ";
            die();
        }
    }

    public function listUsers()
     {
           $users = DB::table('roles')
        ->join('users', 'role_id', '=', 'roles.id')
        ->select('users.*')
        ->orderBy('users.id', 'asc')
        ->get();
// Los ordeno por id
       // $users = User::all()->load('roles');
        return response()->json(array(
            'usuarios' => $users,
            'status' => 'success'
        ), 200);
    }

    public function storeWorkers(Request $request)
    {
        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {

            //Recoger los datos por post
            $json = $request->input('json', null);
            $params = json_decode($json);

            //Guardar el trabajador
            if (isset($params->name) && isset($params->email) && isset($params->password) && isset($params->role_id)) {
                $user = new User();
                $user->name = $params->name;
                $user->email = $params->email;
                $user->password = hash('sha256', $params->password);
                $user->Activo = true;
                $user->role_id = $params->role_id;
                $user->save();

                $data = array(
                    'user' => $user,
                    'status' => 'success',
                    'code' => 200,
                );
            }
        } else {
            //Devolver error
            $data = array(
                'message' => 'Login Incorrecto',
                'status' => 'error',
                'code' => 400,
            );
        }

        return response()->json($data, 200);
    }

    //Metodo para volver a dar de alta a un trabajador
    public function activarWorkers( Request $request)
    {
        $jwtAuth = new JwtAuth();
        $hash = $request->header('Authorization', null);
       // var_dump($hash);

        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //recoger parametros post

            $json = $request->input('json', null);

            $params = json_decode($json);

            $id = (!is_null($json) && isset($params->id)) ? $params->id : null;


            //compruebo el usuario
            $user = User::where(
                array(
                    'id' => $id,
                )
            )->first();
            $user->Activo =1;
            $user->save();

            $data = array(
                'Usuario' => $user,
                'status' => 'success',
                'code' => 200,
            );
        } else {
            $data = array(
                'message' => 'Error al dar de alta Incorrecto',
                'status' => 'error',
                'code' => 400,
            );
        }

        return response()->json($data, 200);
    }

    public function delete(Request $request)
    {
            //FALTA EL TOKEN
            $hash = $request->header('authorization');
            // var_dump($hash);
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
            // var_dump($checkToken);
            if ($checkToken) {
                    //recoger parametros post

            $json = $request->input('json', null);

            $params = json_decode($json);

            $id = (!is_null($json) && isset($params->id)) ? $params->id : null;


            $user = User::where(
                array(
                    'id' => $id,
                )
            )->first();
            $user->Activo =0;
            $user->save();
            $data = array(
                'Usuario' => $user,
                'status' => 'success',
                'code' => 200,
            );
            } else {
                $data = array(
                    'message' => 'Error al darlo de baja Incorrecto',
                    'status' => 'error',
                    'code' => 400,
                );
            }

            return response()->json($data, 200);
        }

    public function update($id, Request $request)
    {
        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //recoger parametros post

            $json = $request->input('json', null);
            $params = json_decode($json);

            $name = (!is_null($json) && isset($params->name)) ? $params->name : null;
            $email = (!is_null($json) && isset($params->email)) ? $params->email : null;
            $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
            $role = (!is_null($json) && isset($params->role_id)) ? $params->role_id : null;



            $pwd = hash('sha256', $password);
            $user1=User::where(
                array(
                    'id' => $id,
                )
            )->first();
            $pwd1=$user1->password;
            //var_dump($pwd1);

            if($pwd1===$password){
                $user = User::where(
                    array(
                        'id' => $id,
                    )
                )->first();
                $user->name = $name;
                $user->email = $email;
                //$user->password = $pwd;
                // $user->Activo = 1;
                $user->role_id = $role;
                $user->save();
            }else{

                $user = User::where(
                    array(
                        'id' => $id,
                    )
                )->first();
                $user->name = $name;
                $user->email = $email;
                $user->password = $pwd;
                // $user->Activo = 1;
                $user->role_id = $role;
                $user->save();
            }
            $data = array(
                'tarea' => $user,
                'status' => 'success',
                'code' => 200,
            );
        } else {
            $data = array(
                'message' => 'Actualizado Incorrecto',
                'status' => 'error',
                'code' => 400,
            );
        }

       return response()->json($data, 200);
    }
}
