<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Helpers\JwtAuth;
use App\Tarea_Realizada;
use Illuminate\Support\Facades\Auth;use Illuminate\Support\Facades\DB;

class TrabajadoresController extends Controller
{
    public function index($id,Request $request)
    {
        // $hash = $request->header('authorization', null);

        // $jwtAuth = new JwtAuth();
        // $checkToken = $jwtAuth->checkToken($hash);

        // if ($checkToken) {
            //Si no es el token del usuario no puede acceder a sus datos:)
        //    $id=$jwtAuth->User($hash);
        //     $user=$id->sub;
        //Consulta al modelo user para sacar los ususarios que son trabajadores
        $Tarea = DB::table('users')
        ->join('tarea_realizada', 'user_id', '=', 'users.id')
        ->join('tareas', 'tarea_realizada.id_tarea', '=', 'tareas.id')
        ->select('tarea_realizada.*', 'tareas.tarea')
        ->where('user_id', '=', $id)
        ->orderBy('tarea_realizada.fecha_realizada', 'desc')
        ->get();
        $data = array(
            'Tarea' => $Tarea,
            'status' => 'success',
            'code' => 200,
        );
        // }else{
        // //Devolver error
        // $data = array(
        //     'message' => 'Login Incorrecto',
        //     'status' => 'error',
        //     'code' => 400,
        // );

        //  }
        return response()->json($data, 200);

    }
    //Metodo para guardar las tareas
    public function storeTareas(Request $request)
    {

        $hash = $request->header('authorization', null);
        //var_dump($hash);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
        //var_dump($checkToken);
        if ($checkToken) {
            $user = $jwtAuth->checkToken($hash,true);
           //var_dump($user->sub);
           //Recoger los datos por post
             $json = $request->input('json', null);
            $params = json_decode($json);

            //Guardar el trabajador

                $tarea = new Tarea_Realizada();
                $tarea->id_tarea = $params->id_tarea;
                $tarea->user_id = $user->sub;
                $tarea->fecha_realizada = $params->fecha_realizada;
                if($params->horas >8){
                    $params->horas=8;
                }
                $tarea->horas = $params->horas;
                $tarea->save();

                $data = array(
                    'Tarea' => $tarea,
                    'status' => 'success',
                    'code' => 200,
                );

        } else {
            //Devolver error
            $data = array(
                'message' => 'Login Incorrecto',
                'status' => 'error',
                'code' => 400,
            );
        }

        return response()->json($data, 200);
    }

    //Metodo para actualizar las tareas
    public function update($id, Request $request)
    {
        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //recoger parametros post

            $json=$request->input('json',null);
            $params=json_decode($json);
            $params_array= json_decode($json,true);

            //actualizar el registro de tareas
            $tarea=Tarea_Realizada::where('id',$id)->update($params_array);
            //debo contemplar que en el cliente no me pueda modificar el user_id y en el cliente aplicar que el maximo sean 8 horas

            $data = array(
                'tarea' => $tarea,
                'status' => 'Actualizado Correcto',
                'code' => 200,
            );

        } else {
            $data = array(
                'message' => 'Actualizado Incorrecto',
                'status' => 'error',
                'code' => 400,
            );
        }

        return response()->json($data, 200);
    }

}
