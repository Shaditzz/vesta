<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea_Realizada extends Model
{
    protected $table="tarea_realizada";
    public function user(){
        return $this->belongsTo('App\user', 'user_id');
    }
}
