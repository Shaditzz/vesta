<?php

namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\User;

class JwtAuth
{
    public $key;
    //Clave secreta para codificar el jwt
    public function __construct()
    {
        $this->key = 'alumn@123456';
    }
    public function signup($email, $password, $getToken = null)
    {
        $signup = false;
        //buscamos el usuario por email y contraseña y solo cojemos el primero
        $user = User::where(
            array(
                'email' => $email,
                'password' => $password
            )
        )->first();

        //Comprobar si el usuario es un objeto
        if (is_object($user)) {
            $signup = true;
        }

        if ($signup) {
            //Genero el token y lo devuelvo

            $token = array(
                'sub' => $user->id,
                'role'=>$user->role_id,
                'email' => $user->email,
                'activo' => $user->Activo,
                'name' => $user->name,
                'iat' => time(), //cuando se ha sacado
                'exp' => time() + (7 * 24 * 60 * 60) //duracion de expiracion de una semana
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');

            $decoded = JWT::decode($jwt, $this->key, array('HS256'));

            if (is_null($getToken)) {
                return $jwt;
            } else {
                return $decoded;
            }
        } else {
            //Devuelvo error
            return array('status' => 'error', 'message' => 'Login ha fallado!!');
        }
    }


    public function checkToken($jwt, $getIdentity = false)
    {
        $auth = false;

        try {
            $decoded = JWT::decode($jwt, $this->key, array('HS256'));
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        }

        if (isset($decoded) && is_object($decoded) && isset($decoded->sub)) {
            $auth = true;
        } else {
            $auth = false;
        }

        if ($getIdentity) {
            return $decoded;
        }

        return $auth;
    }

    public function User($jwt){
        $decoded = JWT::decode($jwt, $this->key, array('HS256'));

        return $decoded;
    }
}
