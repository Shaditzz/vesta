<?php


// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'api'], function () {
    Route::post('/login', 'UserController@login');
    Route::post('/register', 'UserController@register');
    Route::get('/list', 'TareasController@getTareas');//Sacar todo el listasdo de t
    Route::get('/roles', 'TareasController@getRoles');//Sacar todo el listasdo de t
    Route::post('/update/{id}','UserController@update' );
    Route::group(['prefix' => 'admin'], function () {
        // Rutas con el prefijo api/admin
        Route::get('index', 'AdminController@index');
        Route::get('listado', 'AdminController@listUsers'); //listado users
        Route::post('store', 'AdminController@storeWorkers'); //Guardar trabajadores y empresarios
        Route::post('delete/', 'AdminController@delete'); //Dar de baja trabajadores y/o empresarios
        Route::post('update/{id}', 'AdminController@update'); //Actualizar datos de trabajadores y empresarios
        Route::post('activar/', 'AdminController@activarWorkers'); //Dar de alta a empresarios o y/o trabajadores
    });

    Route::group(['prefix' => 'empresario'], function () {
        // Rutas con el prefijo api/trabajadores
        Route::get('listado', 'EmpresarioController@index');//Devuelve los trabajadores
        Route::get('listado/{id}', 'EmpresarioController@show');//Accede a las tareas de un trabajador en concreto
        Route::post('delete/{id}', 'EmpresarioController@deleteWorkers');//Das de baja a un trabajador
        Route::post('update/{id}', 'EmpresarioController@update');//Actualizas los datos de un trabajador
        Route::post('activar/{id}', 'EmpresarioController@activarWorkers');//Dar de alta a trabajadores
    });

    Route::group(['prefix' => 'trabajadores'], function () {
        // Rutas con el prefijo api/trabajadores
        Route::get('listado/{id}', 'TrabajadoresController@index'); //Consultar si puedo quitar el id//Conseguido
        Route::post('store', 'TrabajadoresController@storeTareas');//Guardar tareas
        Route::post('update/{id}', 'TrabajadoresController@update');//Actualizar dicha tarea
    });
});

//Route::post('/api/register', 'UserController@register'); Auth::routes(['register' => false]);
