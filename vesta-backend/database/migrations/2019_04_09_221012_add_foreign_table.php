<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tarea_realizada', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('id_tarea')->references('id')->on('tareas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tarea_realizada', function (Blueprint $table) {
            $table->dropForeign(['id_tarea']);
            $table->dropForeign(['user_id']);
        });
    }
}
