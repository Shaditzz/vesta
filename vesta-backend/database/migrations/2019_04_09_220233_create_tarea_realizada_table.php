<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTareaRealizadaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarea_realizada', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_tarea')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->date('fecha_realizada');
            $table->integer('horas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarea_realizada');
    }
}
