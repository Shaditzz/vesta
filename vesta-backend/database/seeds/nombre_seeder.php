<?php

use Illuminate\Database\Seeder;

use App\Tarea;

class nombre_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tarea=new Tarea();
        $tarea->tarea = 'Programar';
        $tarea->save();

        $tarea=new Tarea();
        $tarea->tarea = 'Arreglar Equipos informaticos';
        $tarea->save();

        $tarea=new Tarea();
        $tarea->tarea = 'Consultoría';
        $tarea->save();
    }
}
