<?php

use Illuminate\Database\Seeder;
use App\Tarea_Realizada;

class Tarea_realizada_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tarea_realizada=new Tarea_Realizada();

        $tarea_realizada->id_tarea = 1;
        $tarea_realizada->user_id = 3;
        $tarea_realizada->fecha_realizada = '2019-04-17 16:04:18';
        $tarea_realizada->horas=8;
        $tarea_realizada->save();

        $tarea_realizada=new Tarea_Realizada();
        $tarea_realizada->id_tarea = 2;
        $tarea_realizada->user_id = 3;
        $tarea_realizada->fecha_realizada = '2019-04-18 16:04:18';
        $tarea_realizada->horas=8;
        $tarea_realizada->save();

        $tarea_realizada=new Tarea_Realizada();
        $tarea_realizada->id_tarea = 3;
        $tarea_realizada->user_id = 3;
        $tarea_realizada->fecha_realizada = '2019-04-19 16:04:18';
        $tarea_realizada->horas=8;
        $tarea_realizada->save();

        $tarea_realizada=new Tarea_Realizada();
        $tarea_realizada->id_tarea = 1;
        $tarea_realizada->user_id = 4;
        $tarea_realizada->fecha_realizada = '2019-04-17 16:04:18';
        $tarea_realizada->horas=8;
        $tarea_realizada->save();

         $tarea_realizada=new Tarea_Realizada();
        $tarea_realizada->id_tarea = 2;
        $tarea_realizada->user_id = 4;
        $tarea_realizada->fecha_realizada = '2019-04-18 16:04:18';
        $tarea_realizada->horas=8;
        $tarea_realizada->save();

         $tarea_realizada=new Tarea_Realizada();
        $tarea_realizada->id_tarea = 3;
        $tarea_realizada->user_id = 4;
        $tarea_realizada->fecha_realizada = '2019-04-19 16:04:18';
        $tarea_realizada->horas=8;
        $tarea_realizada->save();

         $tarea_realizada=new Tarea_Realizada();
        $tarea_realizada->id_tarea = 1;
        $tarea_realizada->user_id = 5;
        $tarea_realizada->fecha_realizada = '2019-04-17 16:04:18';
        $tarea_realizada->horas=8;
        $tarea_realizada->save();

         $tarea_realizada=new Tarea_Realizada();
        $tarea_realizada->id_tarea = 2;
        $tarea_realizada->user_id = 5;
        $tarea_realizada->fecha_realizada = '2019-04-18 16:04:18';
        $tarea_realizada->horas=8;
        $tarea_realizada->save();

         $tarea_realizada=new Tarea_Realizada();
        $tarea_realizada->id_tarea = 3;
        $tarea_realizada->user_id = 5;
        $tarea_realizada->fecha_realizada = '2019-04-19 16:04:18';
        $tarea_realizada->horas=8;
        $tarea_realizada->save();

    }
}
