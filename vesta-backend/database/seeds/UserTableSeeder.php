<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'admin')->first();
        $role_empresario = Role::where('name', 'empresario')->first();
        $role_trabajador = Role::where('name', 'trabajador')->first();

        $user = new User();
        $user->name = 'admin';
        $user->email = 'admin@example.com';
        $user->password = hash('sha256','secret');
        $user->Activo = true;
        $user->role_id=$role_admin->id;
        $user->save();

        $user = new User();
        $user->name = 'empresario';
        $user->email = 'empresario@example.com';
        $user->password = hash('sha256','secret');
        $user->Activo = true;
        $user->role_id=$role_empresario->id;
        $user->save();

        $user = new User();
        $user->name = 'Prueba1';
        $user->email = 'prueba1@example.com';
        $user->password = hash('sha256','secret');
        $user->Activo = true;
        $user->role_id=$role_trabajador->id;
        $user->save();

        $user = new User();
        $user->name = 'Prueba2';
        $user->email = 'prueba2@example.com';
        $user->password = hash('sha256','secret');
        $user->Activo = true;
        $user->role_id=$role_trabajador->id;
        $user->save();

        $user = new User();
        $user->name = 'Prueba3';
        $user->email = 'prueba3@example.com';
        $user->password = hash('sha256','secret');
        $user->Activo = true;
        $user->role_id=$role_trabajador->id;
        $user->save();

    }
}
