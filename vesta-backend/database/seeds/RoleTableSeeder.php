<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->description = 'administrador';
        $role->save();

        $role = new Role();
        $role->name = 'empresario';
        $role->description = 'empresario';
        $role->save();

        $role = new Role();
        $role->name = 'trabajador';
        $role->description = 'trabajador';
        $role->save();
    }
}
