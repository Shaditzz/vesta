import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'default',
  templateUrl: './default.component.html',
  providers: [UserService]
})

export class DefaultComponent implements OnInit {

  public title: string;
  constructor() {
    this.title = 'Inicio';

  }

  ngOnInit() {
    //console.log('default.component cargado correctamente');

  }
}
