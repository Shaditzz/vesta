import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../Models/user';
import { UserService } from '../../services/user.service';
import { UsuariosService } from '../../services/usuarios.service';
@Component({
  selector: 'app-panel-empleados',
  templateUrl: './panel-empleados.component.html',
  styleUrls: ['./panel-empleados.component.css'],
  providers: [UsuariosService]
})
export class PanelEmpleadosComponent implements OnInit {
  public page_title: string;
  public user: User;
  public token;
  public identity;
  public status: string;
  public usuarios: User[];

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _Usuarios: UsuariosService
  ) {
    this.page_title = 'Ver Trabajadores';
    this.user = new User(1, '', '', 1, false, '');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.getAllListEmpleados();
  }

  ngOnInit() {
    if (this.identity.activo == 0) {
      localStorage.removeItem('token');
      localStorage.removeItem('identity');
      alert('Usuario dado de Baja');
      this._router.navigate(['login']);
    }
  }
  getAllListEmpleados() {
    this._Usuarios.getEmpleados().subscribe(
      result => {
        this.usuarios = result.usuarios;
        // console.log(this.usuarios);
      },
      error => {
        console.log('Error al obtener la lista');
        this._router.navigate(['home']);
      }
    );
  }
  getInfo(user) {
    localStorage.setItem('user', JSON.stringify(user));
    this._router.navigate(['/user-detail']);
    // se la guardo y posteriormente se la borro
  }
}
