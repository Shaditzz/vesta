import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyTareasRealizadasComponent } from './modify-tareas-realizadas.component';

describe('ModifyTareasRealizadasComponent', () => {
  let component: ModifyTareasRealizadasComponent;
  let fixture: ComponentFixture<ModifyTareasRealizadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyTareasRealizadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyTareasRealizadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
