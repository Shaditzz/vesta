import { Component, OnInit } from '@angular/core';
import { Tarea_Realizada } from '../../Models/tarea_realizada';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { Tarea } from '../../Models/tarea';
import { TareasService } from '../../services/tareas.service';

@Component({
  selector: 'app-modify-tareas-realizadas',
  templateUrl: './modify-tareas-realizadas.component.html',
  styleUrls: ['./modify-tareas-realizadas.component.css'],
  providers: [TareasService]
})
export class ModifyTareasRealizadasComponent implements OnInit {
  public Tarea: Tarea_Realizada;
  public page_title: string;
  public role;
  public token;
  public identity;
  public modify;

  public tarea: Tarea[];
  public d: Date = new Date();
  constructor(
    private _router: Router,
    private _userService: UserService,
    private _Tareas: TareasService
  ) {
    this.page_title = 'Modificar';
    this.Tarea = JSON.parse(localStorage.getItem('tarea'));
    localStorage.removeItem('tarea');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    // console.log(this.Tarea);
    this.getAllListTarea();
  }

  ngOnInit() {
    if (this.identity.activo === 0) {
      localStorage.removeItem('token');
      localStorage.removeItem('identity');
      alert('Usuario dado de Baja');
      this._router.navigate(['login']);
    }
  }
  getAllListTarea() {
    this._Tareas.getTareas().subscribe(
      result => {
       // console.log(result.tareas);
        this.tarea = result.tareas;


      },
      error => {
        alert('Error al obtener la lista');
      }
    );
  }
  onSubmit(form) {
    this.Tarea = new Tarea_Realizada(this.Tarea.id, form._directives[0].model, this.identity.sub, form._directives[2].model, form._directives[1].model);
    if (this.Tarea.horas > 8) {
      this.Tarea.horas = 8;
    }
    // console.log(this.Tarea);
    this._userService.ModificarTarea(this.Tarea).subscribe(
      response => {
        if (response.status = 'success') {
          this.Tarea = new Tarea_Realizada(1, 1, 1, this.d, 8);
          this._router.navigate(['all-registro']);
        } else {
          alert('Error');
        }
      },
      error => {
        console.log(<any> error);
      }
    );
  }
}
