import { Component, OnInit } from '@angular/core';
import { User } from '../../Models/user';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-creacion-usuarios',
  templateUrl: './creacion-usuarios.component.html',
  styleUrls: ['./creacion-usuarios.component.css'],
  providers: [UserService]

})
export class CreacionUsuariosComponent implements OnInit {
  public user: User;
  public page_title: string;
  public token;
  public identity;
  public role;
  constructor(
    private _router: Router,
    private _userService: UserService,
  ) {
    this.page_title = 'Ver Usuarios';
    this.user = new User(1, '', '', 1, false, '');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.getRoles();
  }

  ngOnInit() {
    if (this.identity.activo == 0) {
      localStorage.removeItem('token');
      localStorage.removeItem('identity');
      alert("Usuario dado de Baja");
      this._router.navigate(['login']);
    }
  }
  onSubmit() {
    this._userService.register(this.user).subscribe(

      response => {

          this.user = new User(1, '', '', 1, false, '');
          this._router.navigate(['/admin-users']);

      },
      error => {
        console.log(<any>error);
      }
    );
  }
  getRoles() {
    this._userService.getRoles().subscribe(
      result => {
        // console.log(result.roles);
        this.role = result.roles;
      },
      error => {
        alert("Error al obtener la lista");
      }
    );
  }

}
