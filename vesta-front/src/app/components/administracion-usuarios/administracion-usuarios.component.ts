import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../Models/user';
import { UserService } from '../../services/user.service';
import { UsuariosService } from '../../services/usuarios.service';
import { ExcelService } from '../../services/excel.service';

@Component({
  selector: 'app-administracion-usuarios',
  templateUrl: './administracion-usuarios.component.html',
  styleUrls: ['./administracion-usuarios.component.css'],
  providers: [UsuariosService]
})
export class AdministracionUsuariosComponent implements OnInit {
  public page_title: string;
  public user: User;
  public token;
  public identity;
  public status: string;
  public usuarios: User[];

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _Usuarios: UsuariosService,
    private excelService: ExcelService
  ) {
    this.page_title = 'Ver Usuarios';
    this.user = new User(1, '', '', 1, false, '');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.getAllListUsers();


  }

  getAllListUsers() {
    this._Usuarios.getUsers().subscribe(
      result => {
        //console.log(result);
        this.usuarios = result.usuarios;
        //console.log(this.usuarios);
      },
      error => {
        //console.log('Error al obtener la lista');
        this._router.navigate(['home']);
      }
    );
  }
  Modificar(user) {
    localStorage.setItem('user', JSON.stringify(user));
    this._router.navigate(['/modify-users']);
  }
  Desactivar(user) {
    this._userService.desactivar(user).subscribe(
      response => {
        // console.log(response);
        if (response.status = 'success') {
          console.log('Well');
          user.Activo = 0;
        } else {
          alert('Error');
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  Activar(user) {
    this._userService.cambiarActivo(user).subscribe(
      response => {
        //console.log(response);
        if (response.status = 'success') {
          //console.log('Well');
          user.Activo = 1;
        } else {
          alert('Error');
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  ngOnInit() {

    if (this.identity.activo == 0) {
      localStorage.removeItem('token');
      localStorage.removeItem('identity');
      alert("Usuario dado de Baja");
      this._router.navigate(['login']);

    }

  }
  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.usuarios, 'sample');
  }
}
