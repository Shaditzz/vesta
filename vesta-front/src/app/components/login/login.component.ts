import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../Models/user';
import { FormsModule } from '@angular/forms';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  providers: [UserService]
})

export class LoginComponent implements OnInit {

  public title: string;
  public user: User;
  public token;
  public identity;
  public status: string;
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) {
    this.title = 'Identificate';
    this.user = new User(1, '', '', 1, false, '');
  }

  ngOnInit() {
    console.log('login.component cargado correctamente');
    // Quiero obtener el nombre del usuario identificado
    // let user = this._userService.getIdentity();
    // console.log(user.name);
    this.logout();

  }

  onSubmit(form) {
    console.log(this.user);

    this._userService.signUp(this.user).subscribe(
      response => {
        if (response.status !== 'error') {
            this.status = 'success';
            // Guardo el token en el web storage
            this.token = response;
            localStorage.setItem('token', this.token);

            // Objeto del usuario identificado
            this._userService.signUp(this.user, true).subscribe(
              response => {
                // Objeto del usuario identificado
                this.identity = response;
                localStorage.setItem('identity', JSON.stringify(this.identity));
                //console.log(this.identity);
                // redireccion tengo que poner aqui que me vaya a la vista principal del rol

                if (this.identity.role === 1 ) {
                  this._router.navigate(['admin-users']);
                } else if (this.identity.role === 2 ) {
                  this._router.navigate(['empleados']);
                } else if (this.identity.role === 3 ) {
                  this._router.navigate(['all-registro']);
                }


              },
              error => {
                console.log(<any> error);
              }
            );

            //console.log(response);
      } else {
        this.status = 'error';
      }
      },
      error => {
        console.log(<any> error);
      }
    );
  }

  logout() {
    this._route.params.subscribe( params => {
      let logout = + params['sure'];
      if (logout === 1) {
        localStorage.removeItem('identity');
        localStorage.removeItem('token');

        this.identity = null;
        this.token = null;

        // redireccion a otro componente
        this._router.navigate(['home']);
      }
    });
  }

}
