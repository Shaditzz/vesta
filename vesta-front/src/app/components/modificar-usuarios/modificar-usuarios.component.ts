import { Component, OnInit } from '@angular/core';
import { User } from '../../Models/user';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-modificar-usuarios',
  templateUrl: './modificar-usuarios.component.html',
  styleUrls: ['./modificar-usuarios.component.css'],
  providers: [UserService]
})
export class ModificarUsuariosComponent implements OnInit {

  public user: User;
  public page_title: string;
  public role;
  public token;
  public identity;
  public modify;
  constructor(
    private _router: Router,
    private _userService: UserService,
  ) {
    this.page_title = 'Modificar';
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    // console.log(this.user.id);
    localStorage.removeItem('user');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.getRoles();
  }
  ngOnInit() {
    if (this.identity.activo == 0) {
      localStorage.removeItem('token');
      localStorage.removeItem('identity');
      alert('Usuario dado de Baja');
      this._router.navigate(['login']);
    }
  }
  onSubmit() {
    this._userService.Modificar(this.user).subscribe(
      response => {
        if (response.status = 'success') {
          this.user = new User(1, '', '', 1, false, '');
          this._router.navigate(['admin-users']);
        } else {
          alert('Error');
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  getRoles() {
    this._userService.getRoles().subscribe(
      result => {
        // console.log(result.roles);
        this.role = result.roles;
      },
      error => {
        alert('Error al obtener la lista');
      }
    );
  }
}
