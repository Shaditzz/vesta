import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UsuariosService } from '../../services/usuarios.service';
import { Tarea } from '../../Models/tarea';
import { User } from '../../Models/user';
import { ExcelService } from '../../services/excel.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
  providers: [UsuariosService]
})
export class UserDetailComponent implements OnInit {
  public page_title: string;
  public tarea: Tarea;
  public token;
  public identity;
  public status: string;
  private user: User;
  public tareas: Tarea[];

  public d: Date = new Date();
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _Usuarios: UsuariosService,
    private excelService: ExcelService
  ) {
    this.user = JSON.parse(localStorage.getItem('user'));
     // console.log(this.user);
    localStorage.removeItem('user');
    this.page_title = 'Ver información del trabajador: ' + this.user.name;
    this.tarea = new Tarea(1, 1, 1, this.d, 8, '', '');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.getAllTareaEmpleados();
  }

  ngOnInit() {
    if (this.identity.activo === 0) {
      localStorage.removeItem('token');
      localStorage.removeItem('identity');
      alert('Usuario dado de Baja');
      this._router.navigate(['login']);

    }

  }
  getAllTareaEmpleados() {
    this._Usuarios.getTareas(this.user.id).subscribe(
      result => {
        //console.log(result.usuario);
        this.tareas = result.usuario;
        // console.log(this.tareas);
      },
      error => {
        console.log('Error al obtener la lista');
        this._router.navigate(['home']);
      }
    );
  }
  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.tareas, 'sample');
 }
}
