import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoTareasRealizadasComponent } from './listado-tareas-realizadas.component';

describe('ListadoTareasRealizadasComponent', () => {
  let component: ListadoTareasRealizadasComponent;
  let fixture: ComponentFixture<ListadoTareasRealizadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoTareasRealizadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoTareasRealizadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
