import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UsuariosService } from '../../services/usuarios.service';
import { Tarea } from '../../Models/tarea';
import { User } from '../../Models/user';
import {ExcelService} from '../../services/excel.service';

@Component({
  selector: 'app-listado-tareas-realizadas',
  templateUrl: './listado-tareas-realizadas.component.html',
  styleUrls: ['./listado-tareas-realizadas.component.css'],
  providers: [UsuariosService]
})
export class ListadoTareasRealizadasComponent implements OnInit {
  public page_title: string;
  public tarea: Tarea;
  public token;
  public identity;
  public status: string;
  private user: User;
  public tareas: Tarea[];
  public d: Date = new Date();

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _Usuarios: UsuariosService,
    private excelService : ExcelService
  ) {
    this.page_title = 'Listado de tareas realizadas';
    this.tarea = new Tarea(1, 1, 1, this.d, 8, '', '');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.user = this.identity;
    this.getAllTareaEmpleados();
  }

  ngOnInit() {
    if (this.identity.activo == 0) {
      localStorage.removeItem('token');
      localStorage.removeItem('identity');
      alert("Usuario dado de Baja");
      this._router.navigate(['login']);
    }
  }
  getAllTareaEmpleados() {
    //console.log(this.identity.sub);
    this._Usuarios.getUserTareas(this.identity.sub).subscribe(
      result => {
        //console.log(result);
        this.tareas = result.Tarea;
        // console.log(this.tareas);
      },
      error => {
        console.log('Error al obtener la lista');
        this._router.navigate(['home']);
      }
    );
  }
  getInfo(tarea) {
    localStorage.setItem('tarea', JSON.stringify(tarea));
    this._router.navigate(['editar-registro']);

    // se la guardo y posteriormente se la borro
  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.tareas, 'sample');
 }
}
