import { Component, OnInit } from '@angular/core';
import { User } from '../../Models/user';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-modify-password',
  templateUrl: './modify-password.component.html',
  styleUrls: ['./modify-password.component.css'],
  providers: [UserService]
})
export class ModifyPasswordComponent implements OnInit {

  public user: User;
  public page_title: string;
  public role;
  public token;
  public identity;
  public modify;
  constructor(
    private _router: Router,
    private _userService: UserService,
  ) {
    this.page_title = 'Modificar';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.user = new User(1, '', '', 1, false, '');
  }

  ngOnInit() {
    if (this.identity.activo === 0) {
      localStorage.removeItem('token');
      localStorage.removeItem('identity');
      alert('Usuario dado de Baja');
      this._router.navigate(['login']);
    }
  }
  onSubmit(form) {
    // console.log(form._directives[0].model);
    this.user=new User(1, '', '', 1, false, form._directives[0].model);
    this._userService.ModificarPassword(this.identity.sub, this.user).subscribe(
      response => {
        if (response.status = 'success') {
          this.user = new User(1, '', '', 1, false, '');
          this._router.navigate(['admin-users']);
        } else {
          alert('Error');
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
