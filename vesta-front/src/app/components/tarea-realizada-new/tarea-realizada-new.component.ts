
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { Tarea } from '../../Models/tarea';
import { TareaSDefault } from '../../Models/tareaSDefault';
import { Target } from '@angular/compiler';
import { TareasService } from '../../services/tareas.service';
import { from } from 'rxjs';
import { Tarea_Realizada } from '../../Models/tarea_realizada';

@Component({
  selector: 'app-tarea-realizada-new',
  templateUrl: './tarea-realizada-new.component.html',
  styleUrls: ['./tarea-realizada-new.component.css'],
  providers: [TareasService]
})
export class TareaRealizadaNewComponent implements OnInit {
  public page_title: string;
  public identity;
  public token;
  public tarea: Tarea[];
  public tarea_rea: Tarea_Realizada;
  public d: Date = new Date();
  public fecha = [];
  verSeleccion: string = '';
  opcionSeleccionado: string = '0';
  // public list : Array<{id: number, text: string}>;
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _Tareas: TareasService
  ) {
    this.page_title = 'Crear un nuevo registro';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.fecha = this.d.toLocaleString().split(" ", 1);
    this.tarea_rea = new Tarea_Realizada(1, 1, 1, this.fecha[0], 8);
    this.getAllListTarea();

  }
  getAllListTarea() {
    this._Tareas.getTareas().subscribe(
      result => {
        this.tarea = result.tareas;
        console.log(this.tarea);

      },
      error => {
        alert("Error al obtener la lista");
      }
    );
  }

  ngOnInit() {

    if (this.identity.activo === 0) {
      localStorage.removeItem('token');
      localStorage.removeItem('identity');
      alert('Usuario dado de Baja');
      this._router.navigate(['login']);

    }

  }
  capturar(opcionSeleccionado) {
    // Pasamos el valor seleccionado a la variable verSeleccion
    console.log(this.opcionSeleccionado)
  }
  onSubmit(form) {

    //console.log(form._directives[2].model);
    this.tarea_rea = new Tarea_Realizada(1, form._directives[0].model, this.identity.sub, form._directives[2].model, form._directives[1].model);
    // console.log(this.tarea_rea);//dia y mes
    this._userService.saveTarea(JSON.stringify(this.tarea_rea)).subscribe(
      response => {
        if (response.status = "success") {
          //this.tarea_rea= new Tarea_Realizada(1,1,1,this.d,8);
          form.reset();
        } else {
          alert("Error");
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
