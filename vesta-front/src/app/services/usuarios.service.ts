import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()

export class UsuariosService {
  public url: string;

  constructor(
    public _http: HttpClient
  ) {
    this.url = GLOBAL.url;
  }

  getUsers(): Observable<any> {
    return this._http.get(this.url + 'admin/listado');
  }
  getEmpleados(): Observable<any> {
    return this._http.get(this.url + 'empresario/listado');
  }
  getTareas(id): Observable<any> {
    return this._http.get(this.url + 'empresario/listado/' + id);
  }
  getUserTareas(id): Observable<any> {
    return this._http.get(this.url + 'trabajadores/listado/' + id);
  }
  crearUsuarios(user, getToken = null): Observable<any> {
    if (getToken != null) {
      user.getToken = 'true';
    }
    let json = JSON.stringify(user);
    let params = 'json=' + json;
    // console.log(params);
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    // console.log(headers);
    return this._http.post(this.url + 'register', params, { headers: headers });
  }
}


