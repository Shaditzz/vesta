import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()

export class TareasService {
  public url: string;


  constructor(
    public _http: HttpClient
  ) {
    this.url = GLOBAL.url;
  }

  getTareas(): Observable<any> {
    return this._http.get(this.url + 'list');
  }
}


