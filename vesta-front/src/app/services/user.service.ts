import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { User } from '../Models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tarea_Realizada } from '../Models/tarea_realizada';

@Injectable()
export class UserService {
  public url: string;
  public identity;
  public token;

  constructor(
    public _http: HttpClient
  ) {
    this.url = GLOBAL.url;
  }

  signUp(user, getToken = null): Observable<any> {
    if (getToken != null) {
      user.getToken = 'true';
    }
    let json = JSON.stringify(user);
    let params = 'json=' + json;
    // console.log(params);
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    // console.log(headers);
    return this._http.post(this.url + 'login', params, { headers: headers });
  }

  // Metodo para sacar la identidad de nuestro usuario desde el localstorage
  getIdentity() {
    let identity = JSON.parse(localStorage.getItem('identity'));
    if (identity !== 'undefined') {
      this.identity = identity;
    } else {
      this.identity = null;
    }
    // Devuelvo la identidad
    // Para acceder a la informacion dentro de un json .
    return this.identity;
  }

  // Metodo para sacar el token desde el localstorage
  getToken() {
    let token = localStorage.getItem('token');

    if (token !== 'undefined') {

      this.token = token;

    } else {

      this.token = null;

    }
    return this.token;
  }
  register(user): Observable<any> {
    let json = JSON.stringify(user);
    let params = 'json=' + json;
    // console.log(params);
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    // console.log(headers);
    return this._http.post(this.url + 'register', params, { headers: headers });

  }
  cambiarActivo(user): Observable<any> {
    let json = JSON.stringify(user);
    let params = 'json=' + json;
    // console.log(params);
    // console.log(user);
// tslint:disable-next-line: max-line-length
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('Authorization', localStorage.getItem('token'));
    //console.log(headers);
    return this._http.post(this.url + 'admin/activar', params, { headers: headers });

  }
  desactivar(user): Observable<any> {
    let json = JSON.stringify(user);
    let params = 'json=' + json;
    //console.log(params);
    //console.log(user);
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('Authorization', localStorage.getItem('token'));
    //console.log(headers);
    return this._http.post(this.url + 'admin/delete', params, { headers: headers });

  }
  Modificar(user): Observable<any> {
    console.log(user);
    let json = JSON.stringify(user);
    let params = 'json=' + json;
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('Authorization', localStorage.getItem('token'));
    //console.log(headers);
    return this._http.post(this.url + 'admin/update/' + user.id, params, { headers: headers });

  }
  ModificarPassword(id,user): Observable<any> {
    console.log(user);
    let json = JSON.stringify(user);
    let params = 'json=' + json;
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('Authorization', localStorage.getItem('token'));
    //console.log(headers);
    return this._http.post(this.url + 'update/' + id, params, { headers: headers });

  }
  ModificarTarea(tarea): Observable<any> {
    // console.log(tarea);
    let json = JSON.stringify(tarea);
    let params = 'json=' + json;
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', localStorage.getItem('token'));

    return this._http.post(this.url + 'trabajadores/update/' + tarea.id, params, { headers: headers });

  }
  getRoles(): Observable<any> {
    return this._http.get(this.url + 'roles');
  }
  saveTarea(tarea): Observable<any> {
    //console.log(token);
    let json = JSON.stringify(tarea);
    let params = 'json=' + tarea;
    //console.log(params);
    //console.log(tarea);
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('Authorization', localStorage.getItem('token'));
    console.log(headers);
    return this._http.post(this.url + 'trabajadores/store', params, { headers: headers });

  }
}

