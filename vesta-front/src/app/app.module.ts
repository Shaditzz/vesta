import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { DefaultComponent } from './components/default/default.component';
import { TareaRealizadaNewComponent } from './components/tarea-realizada-new/tarea-realizada-new.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { AdministracionUsuariosComponent } from './components/administracion-usuarios/administracion-usuarios.component';
import { CreacionUsuariosComponent } from './components/creacion-usuarios/creacion-usuarios.component';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthorizationService } from '../authorization.service';
import { PanelEmpleadosComponent } from './components/panel-empleados/panel-empleados.component';

//MAterial
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ModificarUsuariosComponent } from './components/modificar-usuarios/modificar-usuarios.component';
import { ListadoTareasRealizadasComponent } from './components/listado-tareas-realizadas/listado-tareas-realizadas.component';
import { ModifyTareasRealizadasComponent } from './components/modify-tareas-realizadas/modify-tareas-realizadas.component';
import {ExcelService} from './services/excel.service';
import { ModifyPasswordComponent } from './components/modify-password/modify-password.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DefaultComponent,
    TareaRealizadaNewComponent,
    UserDetailComponent,
    AdministracionUsuariosComponent,
    CreacionUsuariosComponent,
    PanelEmpleadosComponent,
    ModificarUsuariosComponent,
    ListadoTareasRealizadasComponent,
    ModifyTareasRealizadasComponent,
    ModifyPasswordComponent
    ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: jwtTokenGetter,
        //whitelistedDomains: ['localhost:4200']
      }
    }),
    MatSelectModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatButtonModule

  ],
  providers: [
    appRoutingProviders,
    AuthorizationService,
    MatNativeDateModule,
    ExcelService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function jwtTokenGetter() {
  return localStorage.getItem('access_token');
}
