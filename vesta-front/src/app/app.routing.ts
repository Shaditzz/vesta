import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Componentes
import { LoginComponent } from './components/login/login.component';
import { DefaultComponent } from './components/default/default.component';
import { TareaRealizadaNewComponent } from './components/tarea-realizada-new/tarea-realizada-new.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { AdministracionUsuariosComponent } from './components/administracion-usuarios/administracion-usuarios.component';
import { CreacionUsuariosComponent } from './components/creacion-usuarios/creacion-usuarios.component';
import { AuthorizationGuard } from '../authorization.guard';
import { PanelEmpleadosComponent } from './components/panel-empleados/panel-empleados.component';
import { ModificarUsuariosComponent } from './components/modificar-usuarios/modificar-usuarios.component';
import { ListadoTareasRealizadasComponent } from './components/listado-tareas-realizadas/listado-tareas-realizadas.component';
import { ModifyTareasRealizadasComponent } from './components/modify-tareas-realizadas/modify-tareas-realizadas.component';
import { ModifyPasswordComponent } from './components/modify-password/modify-password.component';


const appRoutes: Routes = [
  { path: '', component: DefaultComponent, },
  { path: 'login', component: LoginComponent },
  { path: 'logout/:sure', component: LoginComponent },
  {
    path: 'crear-registro', component: TareaRealizadaNewComponent, canActivate: [AuthorizationGuard], data: {
      allowedRoles: '3'
    }
  },
  {
    path: 'modify-password', component: ModifyPasswordComponent
  },
  { path: 'crear-usuarios', component: CreacionUsuariosComponent },
  {
    path: 'admin-users', component: AdministracionUsuariosComponent, canActivate: [AuthorizationGuard], data: {
      allowedRoles: '1'
    }
  },
  {
    path: 'modify-users', component: ModificarUsuariosComponent, canActivate: [AuthorizationGuard], data: {
      allowedRoles: '1'
    }
  },
  {
    path: 'all-registro', component: ListadoTareasRealizadasComponent, canActivate: [AuthorizationGuard], data: {
      allowedRoles: '3'
    }
  },
  {
    path: 'editar-registro', component: ModifyTareasRealizadasComponent, canActivate: [AuthorizationGuard], data: {
      allowedRoles: '3'
    }
  },
  {
    path: 'empleados', component: PanelEmpleadosComponent, canActivate: [AuthorizationGuard], data: {
      allowedRoles: '2'
    }
  },
  {
    path: 'user-detail', component: UserDetailComponent, canActivate: [AuthorizationGuard], data: {
      allowedRoles: '2'
    }
  },
  { path: '**', component: DefaultComponent }// si no exite la ruta
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
