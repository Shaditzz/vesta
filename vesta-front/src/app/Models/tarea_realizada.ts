export class Tarea_Realizada {
  constructor(
    public id: number,
    public id_tarea: number,
    public user_id: number,
    public fecha_realizada: Date,
    public horas: number,
  ) { }
}
