export class Tarea {
  constructor(
    public id: number,
    public id_tarea: number,
    public user_id: number,
    public fecha_realizada: Date,
    public horas: number,
    public createdAt: any,
    public updatedAt: any
  ) { }
}
